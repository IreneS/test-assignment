const chai = require('chai');
const chaiHttp = require('chai-http');
const faker = require('faker');
const bootsrapApp = require('../src/index');

chai.use(chaiHttp);
const expect = chai.expect;

const createUser = async () => {
  // TODO Please create your random user here and return single created record
};

const clearUsers = async () => {
  // TODO Please clear your database records of users here
};

const createPosts = async (numberOfPostsToCreate) => {
  // TODO Please, create posts here and return result as array of posts
  return [];
};

const clearPosts = async () => {
  // TODO Please clear your database records of posts here
};

const shouldBeError = (response) => {
  expect(response.body).property('type');
  expect(response.body).property('request_id');
  expect(response.body).property('code');
  expect(response.body).property('message');
}

describe('Post Api', () => {
  let app;
  let close;
  beforeEach(async () => {
    const { server, closeServer } = await bootsrapApp();
    app = server;
    close = closeServer;
  });
  describe('GET /posts', () => {
    it('should get empty list of posts', async () => {
      const response = await chai
        .request(app)
        .get('/posts');

      expect(response).status(200);
      expect(response.body).property('has_more').eq(false);
      expect(response.body).property('data').an('array').length(0);
    });
    it('should return one post in the list', async () => {
      const [fakePost] = await createPosts(1);
      const response = await chai
        .request(app)
        .get('/posts');

      expect(response).status(200);
      expect(response.body).property('has_more').eq(false);
      expect(response.body).property('data').an('array').length(1);
      const [testPost] = response.body.data;
      expect(testPost).deep.eq(fakePost);
    });
    it('should return `has_more` true if objects more than limit', async () => {
      await createPosts(21);
      const response = await chai
        .request(app)
        .get('/posts')
        .query({
          limit: 20,
        })

      expect(response).status(200);
      expect(response.body).property('has_more').eq(true);
    });
    it('should return `has_more` false if cannot fetch more objects', async () => {
      await createPosts(20);
      const response = await chai
        .request(app)
        .get('/posts')
        .query({
          limit: 20,
        })

      expect(response).status(200);
      expect(response.body).property('has_more').eq(false);
    });
    it('should paginate to next result', async () => {
      await createPosts(21);
      const { body: allRecords } = await chai
        .request(app)
        .get('/posts')
        .query({
          limit: 21,
        });
      const { body: firstResponse } = await chai
        .request(app)
        .get('/posts')
        .query({
          limit: 10,
        });
      const { body: secondResponse } = await chai
        .request(app)
        .get('/posts')
        .query({
          limit: 10,
          starting_after: firstResponse.data[9].id,
        });
      const { body: thirdResponse } = await chai
        .request(app)
        .get('/posts')
        .query({
          limit: 10,
          starting_after: secondResponse.data[9].id,
        });
      expect(allRecords.data).deep.eq([
        ...firstResponse.data,
        ...secondResponse.data,
        ...thirdResponse.data,
      ]);
    });
    it('should paginate to previous result', async () => {
      await createPosts(21);
      const { body: allRecords } = await chai
        .request(app)
        .get('/posts')
        .query({
          limit: 21,
        });
      const lastRecord = allRecords.data[allRecords.data.length - 1];
      const { body: firstResponse } = await chai
        .request(app)
        .get('/posts')
        .query({
          limit: 10,
          ending_before: lastRecord.id,
        });
      const { body: secondResponse } = await chai
        .request(app)
        .get('/posts')
        .query({
          limit: 10,
          ending_before: firstResponse.data[0].id,
        });
      expect(allRecords.data).deep.eq([
        ...secondResponse.data,
        ...firstResponse.data,
        lastRecord,
      ]);
    });
    it('should allow to change limit of posts', async () => {
      await createPosts(21);
      const response = await chai
        .request(app)
        .get('/posts')
        .query({
          limit: 10,
        });

      expect(response).status(200);
      expect(response.body).property('data').length(10);
    });
    it('if `limit` not passed should use default limit 20', async () => {
      await createPosts(21);
      const response = await chai
        .request(app)
        .get('/posts');

      expect(response).status(200);
      expect(response.body).property('data').length(20);
    });
    it('if `limit` passed less than 1 should use default limit 20', async () => {
      await createPosts(21);
      const response = await chai
        .request(app)
        .get('/posts')
        .query({
          limit: 0,
        });

      expect(response).status(200);
      expect(response.body).property('data').length(20);
    });
    it('if `limit` passed more than 100 should use max limit 100', async () => {
      await createPosts(101);
      const response = await chai
        .request(app)
        .get('/posts')
        .query({
          limit: 101,
        });

      expect(response).status(200);
      expect(response.body).property('data').length(100);
    });
  });
  describe('POST /posts', () => {
    let user = {};
    let postBodyDto = {};

    beforeEach(async () => {
      user = await createUser();
      postBodyDto = {
        title: faker.random.words(),
        user: user.id,
      };
    });
    it('should create post', async () => {
      const response = await chai
        .request(app)
        .post('/posts')
        .send(postBodyDto);

      expect(response).status(201);
      expect(response.body).property('id');
      expect(response.body).property('created_at').an('number');
      expect(response.body).property('updated_at').an('number');
      expect(response.body).property('title').eq(postBodyDto.title);
      expect(response.body).property('author').eq(user.id);
    });
    it('should not create post if `user` missing', async () => {
      const response = await chai
        .request(app)
        .post('/posts')
        .send({
          title: faker.random.words(),
        });

      expect(response).status(400);
      shouldBeError(response);
    });
    it('should not create post if title has length more than 255', async () => {
      const response = await chai
        .request(app)
        .post('/posts')
        .send({
          title: faker.datatype.string(256),
          user: user.id,
        });

      expect(response).status(400);
      shouldBeError(response);
    });
    it('should not create post if title is empty string', async () => {
      const response = await chai
        .request(app)
        .post('/posts')
        .send({
          title: '',
          user: user.id,
        });

      expect(response).status(400);
      shouldBeError(response);
    });
  });
  describe('GET /posts/{id_post}', () => {
    it('should retrieve post one post', async () => {
      const [fakePost] = await createPosts(1);
      const response = await chai
        .request(app)
        .get(`/posts/${fakePost.id}`);
      expect(response).status(200);
      expect(response.body).deep.eq(fakePost);
    });
    it('should return error if post not found', async () => {
      const response = await chai
        .request(app)
        .get('/posts/1');
      expect(response).status(400);
      shouldBeError(response);
    });
  });
  describe('PATCH /posts/{id_post}', async () => {
    const [fakePost] = await createPosts(1);
    it('should partial update post', async () => {
      const updatePostBodyDto = {
        title: faker.random.words(),
      };
      const response = await chai
        .request(app)
        .patch(`/posts/${fakePost.id}`)
        .send(updatePostBodyDto);
      expect(response).status(200);
      expect(response.body).deep.eq({
        ...fakePost,
        ...updatePostBodyDto,
      });
    });
    it('should return error if post not found', async () => {
      const response = await chai
        .request(app)
        .patch(`/posts/1`)
        .send({
          title: faker.random.words(),
        });
      expect(response).status(400);
      shouldBeError(response);
    });
  });
  describe('DELETE /posts/{id_post}', () => {
    it('should completely remove post', async () => {
      const [fakePost] = await createPosts(1);
      const response = await chai
        .request(app)
        .delete(`/posts/${fakePost.id}`);
      expect(response).status(204);
    });
    it('should return no content even if post not found', async () => {
      const response = await chai
        .request(app)
        .delete(`/posts/1`);
      expect(response).status(204);
    });
  });
  afterEach(async () => {
    await clearPosts();
    await clearUsers();
    await close();
  });
});
